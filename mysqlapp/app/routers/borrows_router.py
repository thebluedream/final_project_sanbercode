from app import app
from app.controllers import borrows
from flask import Blueprint, request

borrows_blueprint = Blueprint('borrows_router', __name__)

@app.route("/borrows", methods=["GET"])
def showBorrows():
    return borrows.shows()

@app.route("/borrows/insert", methods=["POST"])
def insertBorrow():
    params = request.json
    return borrows.add(**params)

@app.route("/borrows/status", methods=["POST"])
def chanceBorrow():
    params = request.json
    return borrows.changeStatus(**params)